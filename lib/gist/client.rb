# frozen_string_literal: true

module Gist
	class Client
    	API_URL = 'https://api.getgist.com'
		class << self
			attr_accessor :access_token
			def connect(payload: {}, params: {}, method:, endpoint:)
				uri = URI.parse("#{API_URL}/#{endpoint}")
				uri.query = URI.encode_www_form(params)

				http_method = case method
				when :get
					Net::HTTP::Get
				when :post
					Net::HTTP::Post
				when :patch
					Net::HTTP::Post
				when :delete
					Net::HTTP::Delete
				else
					raise 'invalid method'
				end

				request = http_method.new(uri.request_uri, {'Content-Type' => 'application/json', 'Authorization'=>"Bearer #{@access_token}"})
				request.body = payload.to_json
				response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') { |http| http.request(request) }

				return JSON.parse(response.body) if response.code == '200'

				context = {status: response.code, description: JSON.parse(response.body)}
				case response.code
				when '400'
					raise Gist::BadRequest.new(context)
				when '401'
					raise Gist::InvalidAPIKey.new(context)
				when '403'
					raise Gist::Forbidden.new(context)
				when '404'
					raise Gist::NotFound.new(context)
				when '405'
					raise Gist::MethodNotAllowed.new(context)
				when '422'
					raise Gist::UnprocessableEntity.new(context)
				when '429'
					raise Gist::TooManyRequests.new(context)
				else
					raise Gist::StandardError.new(context)
				end
			end
		end
	end
end
