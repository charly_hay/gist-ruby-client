module Gist
	class Contact
		class << self
			def create_or_update_contact(payload:)
				Gist::Client.connect(payload: payload, method: :post, endpoint: "contacts")
			end
		end
	end
end